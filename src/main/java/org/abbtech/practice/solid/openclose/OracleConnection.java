package org.abbtech.practice.solid.openclose;

public class OracleConnection implements ConnectionService{
    @Override
    public void getConnection() {
        System.out.println("get Oracle Connection");

    }
}
