package org.abbtech.practice.solid.openclose;

public class PostgresqConnection implements ConnectionService {
    @Override
    public void getConnection() {
        System.out.println("get PostgreSql Connection");
    }
}
