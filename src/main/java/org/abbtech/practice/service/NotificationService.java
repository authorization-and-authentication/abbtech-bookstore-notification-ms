package org.abbtech.practice.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.abbtech.practice.entity.Notification;
import org.abbtech.practice.repository.NotificationRepository;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {

    private final NotificationRepository notificationRepository;
    private final ObjectMapper objectMapper;

    public NotificationService(NotificationRepository notificationRepository, ObjectMapper objectMapper) {
        this.notificationRepository = notificationRepository;
        this.objectMapper = objectMapper;
    }

    @KafkaListener(topics = "order-confirm-topic", groupId = "notification-group")
    public void consume(String message) {
        try {
            Notification notification = objectMapper.readValue(message, Notification.class);
            notificationRepository.save(notification);
            sendEmail(notification);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendEmail(Notification notification) {
        String emailContent = String.format("Dear User, your order with ID %s has been confirmed.", notification.getOrderId());
    }
}
